from pythonBasicMock.pythonmock.main import utility

class Animal:
    name = ''
    owners = []

    def __init__(self, name):
        self.name = name
        self.owners = utility.get_owners()

    def scream(self):
        return '??'


class Dog(Animal):
    def scream(self):
        return utility.get_dog_scream()


class Cat(Animal):
    def scream(self):
        return utility.get_cat_scream()
