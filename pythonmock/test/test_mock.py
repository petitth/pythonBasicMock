import unittest
from unittest.mock import patch
from pythonBasicMock.pythonmock.main import utility
from pythonBasicMock.pythonmock.main.animals import *


def faked_scream():
    return 'ahouuuuuuuuuuu'


def faked_owners():
    return ['riri', 'fifi', 'loulou']


class TestMocking(unittest.TestCase):
    def test_utility(self):
        self.assertEqual(utility.get_dog_scream(), 'waf waf')
        self.assertEqual(utility.get_cat_scream(), 'miaou')
        self.assertEqual(utility.get_owners()[0], 'mommy')
        self.assertEqual(utility.get_owners()[1], 'daddy')
        self.assertEqual(len(utility.get_owners()), 2)

    @patch('pythonBasicMock.pythonmock.main.utility.get_dog_scream')
    def test_utility_mock_dog_scream(self, mocked_dog_scream):
        mocked_dog_scream.return_value = faked_scream()
        self.assertEqual(utility.get_dog_scream(), "ahouuuuuuuuuuu")

    def test_utility_mock_dog_scream_v2(self):
        with patch('pythonBasicMock.pythonmock.main.utility.get_dog_scream') as mocked_dog_scream:
            mocked_dog_scream.return_value = faked_scream()
            self.assertEqual(utility.get_dog_scream(), "ahouuuuuuuuuuu")

    def test_mock_multiple(self):
        with patch('pythonBasicMock.pythonmock.main.utility.get_dog_scream') as mocked_dog_scream, \
                patch('pythonBasicMock.pythonmock.main.utility.get_cat_scream') as mocked_cat_scream:
            mocked_dog_scream.return_value = faked_scream()
            mocked_cat_scream.return_value = faked_scream()
            self.assertEqual(utility.get_dog_scream(), "ahouuuuuuuuuuu")
            self.assertEqual(utility.get_cat_scream(), "ahouuuuuuuuuuu")

    @patch('pythonBasicMock.pythonmock.main.utility.get_owners')
    def test_utility_mock_owners(self, mocked_owners):
        mocked_owners.return_value = faked_owners()
        self.assertEqual(utility.get_owners()[0], 'riri')
        self.assertEqual(len(utility.get_owners()), 3)

    @patch('pythonBasicMock.pythonmock.main.utility.get_dog_scream')
    def test_animals_mock_dog_scream(self, mocked_dog_scream):
        mocked_dog_scream.return_value = faked_scream()
        dog = Dog('Wolf')
        self.assertEqual(dog.scream(), 'ahouuuuuuuuuuu')
        cat = Cat('Felix')
        self.assertEqual(cat.scream(), 'miaou')
        self.assertEqual(mocked_dog_scream.call_count, 1)

    @patch('pythonBasicMock.pythonmock.main.utility.get_owners')
    def test_animals_mock_owners(self, mocked_owners):
        mocked_owners.return_value = faked_owners()
        dog = Dog('Rex')
        self.assertEqual(dog.owners[0], 'riri')
        self.assertEqual(mocked_owners.call_count, 1)
        cat = Cat('Felix')
        self.assertEqual(cat.owners[1], 'fifi')
        self.assertEqual(mocked_owners.call_count, 2)

    def test_animals_basic(self):
        dog = Dog('Rex')
        self.assertEqual(dog.scream(), 'waf waf')
        self.assertEqual(dog.owners[0], 'mommy')
        cat = Cat('Felix')
        self.assertEqual(cat.scream(), 'miaou')


if __name__ == "__main__":
    unittest.main()